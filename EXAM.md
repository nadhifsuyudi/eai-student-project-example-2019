# CSCM602023 Advanced Programming (KKI) - 2020 - Final Programming Exam

> Name : Aljihad Ijlal Nadhif Suyudi
> NPM : 1806241021

## Twelve Factors Methodology

1. Codebase: Only have Development at first, then make Staging and master branch become Production.
2. Dependencies: Using Gradle automatic build system.
3. Config: Has been make for environment variables. Just add some new one and add the variables on Heroku.
4. Backing Service: Using dependencies system.
5. Build, Release, run: has been Implemented on gitlab-ci.yml .
6. Processes: - 
7. Port Binding: Has been declared specifically in application.properties .
8. Concurrency: - 
9. Disposability: - 
10. Dev/prod parity: Since the config already made for environment variables, this is also done.
11. Logs: Trying to implement logging
12. Admin processes: has been implemented by spring boot by default.


Heroku Production : https://advprog-finalexam-nadhif.herokuapp.com/
Heroku Staging : https://advprog-final-nadhif-staging.herokuapp.com/